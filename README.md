 # Speech Recognition

This program is to quickly convert an audio file containing speech into text using Google's Speech Recognition.

1. To get started, install Speech Recognition dependency using the following command
`pip install SpeechRecognition`

2. Change the path of the audio file 
	>audiof = ("your-path-here") 

3. You're done, just run the program using
	`python speechrecog.py`

>### Note: Your audio file needs to be in .wav format
Check out this [website](https://www.voiptroubleshooter.com/open_speech/american.html) that offers sample audio clips which you can use for testing.

Check out [documentation for SpeechRecognition](https://pypi.org/project/SpeechRecognition/) library

Credits to *Joy of Computing using Python* course
