import speech_recognition as sr
audiof = ("sample.wav")

r = sr.Recognizer()

with sr.AudioFile(audiof) as source:
    audio = r.record(source)

try:
    print(r.recognize_google(audio))
except sr.UnknownValueError:
    print("Couldn't understand")
except sr.RequestError:
    print("Couldn't get response")

